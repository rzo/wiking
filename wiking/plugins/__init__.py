
def to_bool(val):
    tp = type(val)
    if tp is bool:
        return val
    if tp is int or tp is float:
        return bool(val)
    if tp is str:
        v = val.lower()
        if v in ['true', 'yes', 'on', '1']:
            return True
        if v in ['false', 'no', 'off', '0']:
            return False
        raise ValueError('String "%s" is not a valid bool value' % val)
    raise ValueError('Type %s cannot be converted to bool' % tp)


class CodeBlockPlugin(object):
    def name(self):
        return "name"

    def process(self, page, text):
        return text


class BlockPlugin(object):
    markdown = None

    def name(self):
        return 'name'

    def extra_style(self):
        return ''

    def extra_script(self):
        return ''

    def process(self, body, **kwargs):
        return body


class PluginRegistry(object):
    def __init__(self):
        self._code_block_plugins = {}
        self._block_plugins = {}
        self._fallback_block_plugin_getter = None

    def register_code_block_plugin(self, p):
        self._code_block_plugins[p.name()] = p

    def register_block_plugin(self, p):
        self._block_plugins[p.name()] = p

    def find_code_block_plugin(self, name):
        return self._code_block_plugins[name] if name in self._code_block_plugins else None

    def find_block_plugin(self, name):
        if name in self._block_plugins:
            return self._block_plugins[name]
        if self._fallback_block_plugin_getter is not None:
            return self._fallback_block_plugin_getter(name)
        return None

    def set_fallback_block_pluggin_getter(self, getter):
        self._fallback_block_plugin_getter = getter

    def _gather_text(self, fn):
        return ''.join((fn(p) for p in self._block_plugins.values()))

    def extra_style(self):
        return self._gather_text(lambda p: p.extra_style())

    def extra_script(self):
        return self._gather_text(lambda p: p.extra_script())


registry = PluginRegistry()
