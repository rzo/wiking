from . import CodeBlockPlugin, registry
import subprocess, os.path, base64


class PlantUmlCodeBlockPlugin(CodeBlockPlugin):
    plantUmlJarPath = os.path.join(os.path.dirname(__file__), '..', '..', 'bin', 'plantuml.jar')

    def name(self):
        return "plantuml"

    def process(self, page, text):
        imgData = self.run_plant_uml(text)
        return '<img src="data:image/png;base64,%s"/>' % base64.b64encode(imgData)

    def run_plant_uml(self, text):
        p = subprocess.Popen(['java','-jar',self.plantUmlJarPath, '-p'], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        stdout, stderr = p.communicate(text)
        if p.returncode != 0:
            print(stderr)
            raise RuntimeError('Running plantuml (%s) failed with error code: %d and message: %s', self.plantUmlJarPath, p.returncode, stderr)
        #print(stdout)
        return stdout


def register():
    registry.register_code_block_plugin(PlantUmlCodeBlockPlugin())
