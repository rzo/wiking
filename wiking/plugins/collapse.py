from . import registry, BlockPlugin, to_bool


class CollapseBlockPlugin(BlockPlugin):

    def name(self):
        return 'collapse'

    def extra_style(self):
        return '''
        .collapse { padding: 0.5em; overflow: auto; }
        .collapse > p { display: inline-block; }
        .collapse > button { float: right; display: inline-block; vertical-align: middle; }
        .collapse-area.collapsed { 
            display: none;
        }
        .collapse-area { height: auto; }
        '''

    def extra_script(self):
        return '''
        function collapse_clicked(el, showTitle, hideTitle){ 
            var i, toggled, eli, els = el.parentElement.getElementsByClassName('collapse-area');
            showTitle = showTitle || 'More';
            hideTitle = hideTitle || 'Less';
            for (i = 0; i < els.length; i++) { 
                eli = els[i];
                toggled = eli.classList.toggle('collapsed'); 
            }
            el.innerText = toggled ? showTitle : hideTitle;
        }'''

    def process(self, body, title='', open=False, show_title='More', hide_title='Less'):
        open = to_bool(open)
        extra_class = '' if open else ' collapsed'
        return '''<div class="collapse">{title}<button onclick="collapse_clicked(this,'{show_title}','{hide_title}');">{button_title}</button>
        <div class="collapse-area{extra_class}">
        {body}
        </div>
        </div>'''.format(
            body=body,
            title=self.markdown(title),
            open=open,
            extra_class=extra_class,
            show_title=show_title,
            hide_title=hide_title,
            button_title=hide_title if open else show_title)


def register():
    registry.register_block_plugin(CollapseBlockPlugin())
