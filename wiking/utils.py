import distutils.dir_util
import os
import shutil
import random
import time

def copy2(src, dst):
    if os.path.exists(dst):
        os.remove(dst)
    shutil.copy2(src, dst)


def makedirs(path):
    if not os.path.exists(path):
        os.makedirs(path)


def copytree(src, dst, preserve_symlinks=0, update=0, verbose=0):
    distutils.dir_util.copy_tree(src, dst, preserve_symlinks=preserve_symlinks, update=update, verbose=verbose)


def copydirs(src, dst, preserve_symlinks=0, update=0, verbose=0):
    files = os.listdir(src)
    for f in files:
        srcp = os.path.join(src, f)
        if not os.path.isdir(srcp):
            continue
        dstp = os.path.join(dst, f)
        copytree(srcp, dstp, preserve_symlinks=preserve_symlinks, update=update, verbose=verbose)


def saveManifest(name, type, title, author, template=None):
    extra = ''
    if template is not None:
        extra = ',\n  "template":"%s"\n' % template
    with open(os.path.join(name, 'info.json'), 'wt') as fp:
        fp.write('{\n  "type":"%s",\n  "title":"%s",\n  "author":"%s"%s\n}\n' % (type, title, author, extra))


def findRoot():
    rp = '.'
    ap = os.path.abspath(rp)
    if os.altsep is not None and len(os.altsep) > 0:
        ap = ap.replace(os.altsep, os.sep)
    pathCompos = ap.split(os.sep)
    if 'root' not in pathCompos:
        r = os.path.abspath(os.path.join(rp, 'root'))
        if os.path.exists(r):
            return r
        return None
    idx = pathCompos.index('root')
    return os.sep.join(pathCompos[:idx+1])


def loremIpsum(words_per_sentence=10, sentences_per_paragraph=5, paragraphs=3):
    lexicon=['lacus','laoreet','blandit','elit','malesuada','ligula','eros','tellus','consectetur','sapien','metus',
             'non','congue','nibh','tincidunt','bibendum','nisi','consequat','condimentum','integer','odio','ipsum',
             'neque','mattis','nisl','viverra','augue','porta','lacinia','orci','ultricies','posuere','auctor',
             'sagittis','sed','ex','eu','et','molestie','iaculis','diam','risus','nec','nunc','sem','suscipit',
             'elementum','hendrerit','velit','leo','euismod','magna','curabitur','maecenas','phasellus','amet',
             'aenean','aliquam','vestibulum','pretium','justo','libero','facilisis','nulla','placerat','proin',
             'pharetra','ultrices','varius','purus','imperdiet','vel','venenatis','enim','ante','dui','praesent',
             'quis','cras','interdum','fringilla','morbi','rhoncus','feugiat','luctus','lobortis','vehicula','tempus',
             'quam','ullamcorper','efficitur','donec','suspendisse','accumsan','dapibus','tortor','duis','ut',
             'sollicitudin','mi','egestas','vulputate','maximus','volutpat','sodales','felis','ac','dictum','semper',
             'ornare','lectus','eleifend','erat','at','in','id','urna','nullam','mauris','sit','convallis','nam',
             'pellentesque','massa','dignissim','vitae','pulvinar','arcu','commodo','gravida','turpis','est','cursus',
             'aliquet','lorem','porttitor','a','rutrum','tristique','adipiscing','dolor','finibus','fermentum']

    def capitalize(w):
        return w[0].upper() + w[1:]

    def rnd(n):
        return random.randint(int(n - n * 0.3),
                              int(n + n * 0.3))
    ret = ''
    for i in range(paragraphs):
        st = rnd(sentences_per_paragraph)
        for j in range(st):
            ws = rnd(words_per_sentence)
            for k in range(ws):
                w = lexicon[random.randint(0, len(lexicon) - 1)]
                if k == 0:
                    w = capitalize(w)
                else:
                    ret += ' '
                ret += w
            ret += '. '
        ret += '\n\n'
    return ret

def format_time(tm_secs):
    return time.strftime('%Y-%m-%d %H:%S', time.localtime(tm_secs))
