from unittest import TestCase
import os.path
import model as md

class TestProject(TestCase):
    def __init__(self, methodName=None):
        super(TestProject, self).__init__(methodName)

    def setUp(self):
        super(TestProject, self).setUp()

    def test_simple1(self):
        c = 0
        self._proj_root = os.path.join(os.path.dirname(__file__), '..', 'tests', 'simple1')
        self._proj = md.Project(self._proj_root)
        self._proj.scan()
        self._proj.load()

        for n in self._proj.walk():
            self.assertIn('author', n.metadata, 'Missing author metadata in node: ' + n.path)
            self.assertEqual(n.author, 'Me', 'Error in node: ' + n.path)
            c += 1
        self.assertNotEqual(c, 0)
        root = self._proj.root()
        self.assertIsNotNone(root)
        pg, pg_path = root.resolve('overview')
        print(pg_path)
        self.assertIsNotNone(pg)
        att, att_path = pg.resolve('Colouro-preview-screen.png')
        self.assertIsNotNone(att)

        pg2, pg2_path = root.resolve('space/hello')
        self.assertIsNotNone(pg2)
        print(pg2_path)

        print(root.public_path(), '->', pg.public_path(), ':', root.relative_url(pg))
        print(pg2.public_path(), '->', pg.public_path(), ':', pg2.relative_url(pg))
        print(pg2.public_path(), '->', att.public_path(), ':', pg2.relative_url(att))
        p = pg2.parent #.parent
        print(p.public_path(), '->', pg2.public_path(), ':', p.relative_url(pg2))

