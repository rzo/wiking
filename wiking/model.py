import yaml, json, re, os.path

from wiking import utils

"""
 [ Project ]
 +-- 00_1_Intro/   [Page attachments]
 +-- 00_1_Intro.md [Page ] 
"""


class FileNameError(Exception):
    def __init__(self, path):
        name = os.path.basename(path)
        message = '{}: the file name "{}" is not valid Node name. Rename it to conform format like 00_0_my_name.md.'
        super(FileNameError, self).__init__(message)
        self.path = path
        self.name = name


def is_wiki_dir(path):
    info_files = ('info.yml', 'info.json')
    for info_file in info_files:
        if os.path.exists(os.path.join(path, info_file)):
            return True
    return False


def find_project_root(path):
    p = find_project_wiki_root(path)
    return os.path.dirname(p) if p is not None else None


def find_project_wiki_root(path):
    path = os.path.abspath(path)
    if not is_wiki_dir(path):
        path = os.path.join(path, 'wiki')
        return path if is_wiki_dir(path) else None
    while True:
        npath, dirname = os.path.split(path)
        if dirname == '' or not is_wiki_dir(npath):
            return path


def parse_filepath(path):
    bn = os.path.basename(path)
    bn, ext = os.path.splitext(bn)
    m = re.match(r'([0-9_-]*)(.*)', bn)
    if m is None or len(m.groups()) != 2:
        raise FileNameError(path)
    return m.group(1), m.group(2), ext


def load_yaml(path):
    if os.path.exists(path):
        with open(path) as fp:
            return yaml.load(fp, Loader=yaml.SafeLoader)
    return None


def load_json(path):
    if os.path.exists(path):
        with open(path) as fp:
            return json.load(fp)
    return None


def create_node(project_root, path, parent=None):
    if path.lower().endswith('.md'):
        return MarkdownNode(path, parent)
    elif os.path.isdir(os.path.join(project_root, path)):
        return Node(path, parent)
    else:
        return AttachmentNode(path, parent)


class Node(object):
    def __init__(self, path, parent=None):
        # Parent node (if any)
        self.parent = parent
        # Relative path within project
        self.path = path
        # Time of modification
        self._mod_time = None
        # Type of node (basically denotes template for rendering)
        self.archetype = 'space'
        # Relative path to directory where children are stored
        self.dir_path, ext = os.path.splitext(self.path)
        # Name
        self.name = os.path.basename(self.path)
        # Order prefix to sort accordingly, and commonly used name
        self.order_prefix, self.common_name, self.ext = parse_filepath(self.path)
        # Default title
        self.title = self.common_name
        # Public name (to file/dir)
        self.public_name = self.common_name + self.ext # self.name
        # Public dir name
        # This is important for markdown node as its dir is different from file (rendered html)
        self.public_dir_name = self.common_name
        # Metadata dictionary
        self.metadata = {}
        # Children (attachments)
        self.children = []
        self.public_children = []
        # Map of mirrored properties in metadata
        # Key: metadata key name, Value: self prop name to be set
        self.mirrored_props = {'archetype': 'archetype', 'name': 'public_name', 'title':'title'}
        self.level_offset = 0

    def __getattr__(self, item):
        if item not in self.metadata:
            raise AttributeError()
        return self.metadata[item]

    def mod_time(self):
        return utils.format_time(self._mod_time)

    def public_path(self):
        ret = self.public_name
        n = self.parent
        while n is not None:
            if len(n.public_dir_name) > 0:
                ret = n.public_dir_name + '/' + ret
            n = n.parent
        return ret

    def find_children(self, name):
        lname = name.lower()
        for ch in self.children:
            if ch.name.lower() == lname or ch.common_name.lower() == lname:
                return ch
        return None

    def find_path(self, path):
        n = self
        normalized_path = ''
        while path is not None and len(path) > 0 and n is not None:
            tokens = path.split('/', 1)
            name = tokens[0]
            path = tokens[1] if len(tokens) > 1 else None
            n = n.find_children(name)
            if n is not None:
                pn = n.public_dir_name if path is not None else n.public_name
                if len(pn) > 0:
                    if len(normalized_path) > 0:
                        normalized_path += '/'
                    normalized_path += pn
        return n, (normalized_path if n is not None else None)

    def parent_chain(self):
        ch = [self]
        n = self.parent
        while n is not None:
            ch.insert(0, n)
            n = n.parent
        return ch

    def is_in_parent_chain(self, n):
        p = self
        while p is not None:
            if p == n:
                return True
            p = p.parent
        return False

    def root(self):
        n = self
        while n.parent is not None:
            n = n.parent
        return n

    def root_prefix(self):
        return '../' * (self.level() - 1)

    def level(self):
        i = self.level_offset
        n = self.parent
        while n is not None:
            i += 1
            n = n.parent
        return i

    def public_level(self):
        i = 0
        n = self
        while n is not None:
            if len(n.public_dir_name) > 0:
                i += 1
            n = n.parent
        return i

    def resolve(self, path):
        ch, ch_path = self.find_path(path)
        if ch is not None:
            return ch, self.public_dir_name + '/' + ch_path if len(self.public_dir_name) > 0 else ch_path
        n = self.parent
        relpath = ''
        while n is not None:
            ch, ch_path = n.find_path(path)
            if ch is not None:
                return ch, relpath + ch_path
            if len(n.public_name) > 0:
                relpath += '../'
            n = n.parent
        return None, None

    def relative_url_orig(self, to_node):
        if self == to_node:
            return self.public_name
        other_chain = to_node.parent_chain()
        my_chain = self.parent_chain()
        same_off = 0
        for my, other in zip(my_chain, other_chain):
            if my == other:
                same_off += 1
            else:
                break
        # if same_off > 0:
        #     n = my_chain[same_off - 1]
        #     # if last same has different public_name and public_dir_name
        #     if n.public_name != n.public_dir_name:
        #         same_off -= 1
        real_up_count = sum((len(n.public_dir_name) > 0 for n in my_chain[same_off:])) - 1
        prefix_components = ['..'] * real_up_count
        to_node_components = [n.public_dir_name for n in other_chain[same_off:-1] if len(n.public_dir_name) > 0]
        to_node_components += [to_node.public_name]
        rel_url = '/'.join(prefix_components + to_node_components)
        return rel_url

    def relative_url(self, to_node):
        my_path = self.public_path()
        other_path = to_node.public_path()
        my_path_comps = my_path.split('/')
        other_path_comps = other_path.split('/')
        same_off = 0
        for my_path_comp, other_path_comp in zip(my_path_comps, other_path_comps):
            if my_path == other_path_comp:
                same_off += 1
            else:
                break
        # a/b/c
        # a/b/d
        # d
        prefix = '../' * (len(my_path_comps) - same_off - 1)
        return prefix + '/'.join(other_path_comps[same_off:])

    def scan(self, project_root):
        path = os.path.join(project_root, self.dir_path)
        if not os.path.isdir(path):
            return
        entries = os.listdir(path)
        children = []
        for e in entries:
            if e[0] == '.' or e.lower() in ('info.json', 'info.yml') or (e + '.md') in entries:
                continue
            if e.endswith('.json') and e[:-5] in entries:
                continue
            if e.endswith('.yml') and e[:-4] in entries:
                continue
            node_path = os.path.join(self.dir_path, e)
            n = create_node(project_root, node_path, self)
            n.scan(project_root)
            children.append(n)
        self.children = children
        self._sort_children()
        self._update_public_children()

    def load(self, project_root, meta_data_only=False):
        """Loads metadata and content to be rendered"""
        self._load_metadata(project_root)
        for ch in self.children:
            ch.load(project_root, meta_data_only)

    def walk(self):
        yield self
        nodes = list(self.children)
        while len(nodes) > 0:
            n = nodes.pop()
            nodes.extend(n.children)
            yield n

    def match_name(self, name):
        return name == self.name.lower() \
               or name == self.common_name.lower() \
               or name == (self.common_name + self.ext).lower()

    def _get_metadata_base_path(self):
        return os.path.join(self.dir_path, 'info')

    def _load_metadata(self, project_root):
        base_path = os.path.join(project_root, self._get_metadata_base_path())
        data = load_yaml(base_path + '.yml')
        if data is not None:
            self.metadata = data
            return
        data = load_json(base_path + '.json')
        if data is not None:
            self.metadata = data
            self.update_metadata_props()

    def update_metadata_props(self):
        for m in self.mirrored_props:
            if m in self.metadata:
                setattr(self, self.mirrored_props[m], self.metadata[m])

    def _sort_children(self):
        self.children.sort(key=lambda ch: ch.name)

    def _update_public_children(self):
        self.public_children = [ch for ch in self.children if ch.archetype != 'attachment']


class AttachmentNode(Node):
    def __init__(self, path, parent):
        super(AttachmentNode, self).__init__(path, parent)
        self.archetype = 'attachment'

    def _get_metadata_base_path(self):
        return self.path


class MarkdownNode(Node):
    def __init__(self, path, parent=None):
        super(MarkdownNode, self).__init__(path, parent)
        self.markdown = ''
        self.ext = '.html'
        self.public_name = self.common_name + self.ext
        self.archetype = 'page'
        self.markdown_path = self.path

    def load(self, project_root, meta_data_only=False):
        super(MarkdownNode, self).load(project_root, meta_data_only)
        if meta_data_only or self.markdown_path is None:
            return
        path = os.path.join(project_root, self.markdown_path)
        self._mod_time = os.path.getmtime(path)
        with open(path) as fp:
            lines = fp.readlines()
        if len (lines) > 2 and lines[0].strip() == '---':
            i = 1
            while len(lines) > i and lines[i].strip() != '---':
                i += 1
            self.metadata.update(yaml.load('\n'.join(lines[1:i]), Loader=yaml.SafeLoader))
            self.update_metadata_props()
            lines = lines[i + 1:]
        self.markdown = ''.join(lines)


class Project(MarkdownNode):
    def __init__(self, project_root):
        # Note that in case of Project the path is actually absolute path to project root directory
        super(Project, self).__init__('wiki')
        self.public_name = self.public_dir_name = ''
        self.project_root = project_root        

    def scan(self, project_root=None):
        self._update_project_root(project_root)
        super(Project, self).scan(self.project_root)
        self.markdown_path = None
        found_index = None
        for ch in self.children:
            if ch.match_name('index'):
                found_index = ch
                break
        if found_index is not None:
            self.markdown_path = os.path.join(self.name, found_index.name)
            self.children.remove(found_index)
            self.public_children.remove(found_index)
            self.public_name = 'index.html'
            assert len(found_index.children) == 0, 'No attachment are allowed for project index file %s but %d found' \
                                          % (found_index.name, len(found_index.children))

    def load(self, project_root=None, meta_data_only=False):
        self._update_project_root(project_root)
        super(Project, self).load(self.project_root, meta_data_only)

    def root(self):
        return self

    def _update_project_root(self, project_root):
        if project_root is not None:
            self.project_root = project_root


def print_tree(n, level=0):
    print(' '.join(('  ' * level, n.name, '(%s)' % n.archetype)))
    for ch in n.children:
        print_tree(ch, level + 1)
