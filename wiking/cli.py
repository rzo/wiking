#!/usr/bin/env python2

from . import model as md
from . import renderer as rd
from . import themes
import argparse, os, sys
import shutil


def write_file(dirpath, fn, content):
    with open(os.path.join(dirpath, fn), 'wt') as fp:
        fp.write(content)


class App(object):
    def __init__(self, start_path, load_project=True, verbose=True):
        self.verbose = verbose
        self.root = md.find_project_root(start_path)
        if self.root is None and load_project:
            raise RuntimeError('Cannot find project root')
        self.proj = None
        self.theme = 'minimal'
        self.theme_args = dict()
        if load_project:
            self.proj = md.Project(self.root)
            self.proj.scan()
            self.proj.load()
            if verbose:
                md.print_tree(self.proj)
            self.theme = self.proj.metadata.get('theme', self.theme)
            self.theme_args = self.proj.metadata.get('theme_args', self.theme_args)
        self.theme_dir = os.path.join(themes.path, themes.get_theme_base_name(self.theme))

    def build(self, args):
        output_dir = os.path.abspath(os.path.join(self.root, 'built'))
        if args.out is not None:
            output_dir = args.out
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        themes.copy_extras(self.theme, output_dir, self.theme_args)
        env = rd.create_env(self.theme_dir)
        rd_cache = {}
        for n in self.proj.walk():
            path = os.path.join(output_dir, n.public_path())
            dir_path = os.path.dirname(path)
            if not os.path.exists(dir_path):
                os.makedirs(dir_path)
            type = n.archetype
            if type == 'attachment':
                shutil.copy(os.path.join(self.root, n.path), path)
                continue
            elif type == 'space':
                dir_path = path
                if not os.path.exists(dir_path):
                    os.makedirs(dir_path)
                path = os.path.join(path, 'index.html')
                n.level_offset = 1
            t = type + '.html'
            if t in rd_cache:
                rdr = rd_cache[t]
            else:
                rdr = rd.create_page_renderer(self.theme_dir, t, env)
                rd_cache[t] = rdr
            buf = rdr.render(n)
            with open(path, 'wt') as fp:
                fp.write(buf)

    def init(self, args):
        dirs = [os.path.join(args.project, dn) for dn in ('built', 'wiki')]
        for d in dirs:
            os.makedirs(d)
        wiki_dir = dirs[-1]
        write_file(wiki_dir, 'info.yml', 'theme: minimal\ntitle: "{}"\n'.format(args.title))
        write_file(wiki_dir, 'index.md', '---\ntitle: "{}"\n---\n'.format(args.title))


def create_argument_parser():
    ap = argparse.ArgumentParser(description="Wiking CLI")
    ap.add_argument('-p', '--project', default='.', help='Project path')
    ap.add_argument('-v', '--verbose', default=False, action='store_true', help='Verbose')
    sub = ap.add_subparsers()

    init = sub.add_parser('init')
    init.add_argument('--out', help='Destination of wiki project', default='.')
    init.add_argument('--title', help='Wiki space title', default='wiki')
    init.set_defaults(func=App.init, load_project=False)

    build = sub.add_parser('build')
    build.add_argument('--out', default=None)
    build.set_defaults(func=App.build, load_project=True)

    return ap


def main():
    ap = create_argument_parser()
    args = ap.parse_args(['build'] if len(sys.argv) == 1 else None)
    app = App(args.project, verbose=args.verbose, load_project=args.load_project)
    args.func(app, args)


if __name__ == '__main__':
    main()
