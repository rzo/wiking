import mistune, copy, re, jinja2, time
from . import plugins, utils
from .plugins import collapse
# class MyInlineGrammar(mistune.InlineGrammar):
#     #macro = re.compile(r"\{macro\}(.+?)\{macro\}", re.DOTALL)
#     pass
#
# class MyInlineLexer(mistune.InlineLexer):
#     default_rules = copy.copy(mistune.InlineLexer.default_rules)
#     #default_rules.insert(3, "macro")
#
#     def __init__(self, renderer, rules=None, **kwargs):
#         if rules is None:
#             rules = MyInlineGrammar()
#         super(MyInlineLexer, self).__init__(renderer, rules, **kwargs)
#
#     #def output_macro(self, m):
#     #    text=m.group(1)
#     #    return '<div style="color: #ffff00">' + text + "</div>"
#
#
# def registerMacroProcessor(mp):
#     name = mp.name();
#     setattr(MyInlineGrammar, name, re.compile(r"\{%s\}(.+?)\{%s\}" % (name, name), re.DOTALL|re.MULTILINE))
#     MyInlineLexer.default_rules.insert(3, name)
#     def handleOutput(slf, m):
#         print("Macro", name)
#         return mp.process(m.group(1))
#     setattr(MyInlineLexer, "output_" + name, lambda self, m: mp.process(m.group(1)))
#     #print(dir(MyInlineLexer))


orig_escape = mistune.escape
plugins.collapse.register()

autoformat_replace = [
    (re.compile(r'(?<![-])-{3}(?![-])'), '&mdash;'),
    (re.compile(r'(?<![-])-{2}(?![-])'), '&ndash;'),
    # (re.compile(r'\b-->\b'), '&rarr;'),
    # (re.compile(r'\b->\b'), '&rarr;'),
    # (re.compile(r'\b<--\b'), '&larr;'),
    # (re.compile(r'\b<-\b'), '&larr;'),
]


def new_escape(text, quote=False, smart_amp=True):
    if smart_amp:
        for rx, new in autoformat_replace:
            if type(rx) is str:
                text = text.replace(rx, new)
            else:
                text = re.sub(rx, new, text)
    text = orig_escape(text, quote, smart_amp)
    return text


mistune.escape = new_escape


class CustomBlockLexerMixin(object):
    def enable_custom_block(self):
        self.rules.custom_block = re.compile(r'{{(\w+)([^}]*)}}(.*?){{\/\1}}', re.DOTALL)
        self.default_rules.insert(0, 'custom_block')

    def parse_custom_block(self, m):
        gs = m.groups()
        param_tokens = gs[1]
        param_kvs = re.findall(r'(\w+)="([^"]+)"', param_tokens)
        params = {k.replace('-','_'): v for k,v in param_kvs}
        token = {
            'type': 'custom_block',
            'name': gs[0],
            'body': gs[2],
            'params': params
        }
        self.tokens.append(token)


class CustomBlockMarkdownMixin(mistune.Markdown):
    def __init__(self, my_wiki_rdr=None):
        super(CustomBlockMarkdownMixin, self).__init__(renderer=my_wiki_rdr or MyWikiRenderer(), block=MyWikiBlockLexer)

    def output_custom_block(self):
        name = self.token['name']
        body = self.token['body']
        params = self.token['params']
        markdown = CustomBlockMarkdownMixin(self.renderer)
        text = markdown(body)
        handler = plugins.registry.find_block_plugin(name)
        if handler is None:
            return text
        handler.markdown = markdown
        return handler.process(text, **params)


class MyWikiBlockLexer(CustomBlockLexerMixin, mistune.BlockLexer):
    def __init__(self, *args, **kwargs):
        super(MyWikiBlockLexer, self).__init__(*args, **kwargs)
        self.enable_custom_block()


class MyWikiRenderer(mistune.Renderer):
    def __init__(self, **kwargs):
        super(MyWikiRenderer, self).__init__(**kwargs)
        self._node = None

    def set_node(self, page):
        self._node = page

    def resolve_url(self, src):
        url = src
        if self._node is not None:
            att, relpath = self._node.resolve(src)
            if att is not None:
                url = relpath
        return url

    def block_code(self, code, lang=None):
        p = plugins.registry.find_code_block_plugin(lang)
        if p is not None:
            return p.process(self._node, code)
        else:
            return super(MyWikiRenderer, self).block_code(code, lang)

    def image(self, src, title, text):
        src = self.resolve_url(src)
        if title is None:
            title = text
        return '<div class="figure">' \
               '<figure onclick="this.classList.toggle(\'fullscreen\'); return false;">' \
               '<img src="{src}" alt="{alt}">' \
               '<figcaption>{title}</figcaption>' \
               '</figure>' \
               '</div>'.format(src=src,alt=text,title=title)

    def link(self, link, title, text):
        link = self.resolve_url(link)
        return super(MyWikiRenderer, self).link(link, title, text)

    def autolink(self, link, is_email=False):
        if not is_email:
            link = self.resolve_url(link)
        return super(MyWikiRenderer, self).autolink(link, is_email)

    def header(self, text, level, raw=None):
        return super(MyWikiRenderer, self).header(text, level+1, raw)


class WikiPageRenderer(object):
    def __init__(self, template):
        self._renderer = MyWikiRenderer()
        self._inline = None #MyInlineLexer(self._renderer)
        self._template = template

    def render(self, node):
        self._renderer.set_node(node)
        if hasattr(node, 'markdown'):
            # body = mistune.markdown(node.markdown, renderer=self._renderer, inline=self._inline, block=MyWikiBlockLexer)
            body = CustomBlockMarkdownMixin(self._renderer)(node.markdown)
        else:
            body = ''
        now = utils.format_time(time.time())
        return self._template.render(
            node=node,
            body=body,
            extra_style=plugins.registry.extra_style(),
            extra_script=plugins.registry.extra_script(),
            now=now)


class SpaceRenderer(object):
    def __init__(self, template):
        self._template = template

    def render(self, space):
        return self._template.render(
            space=space,
            now=utils.format_time(time.time()),
            extra_style=plugins.registry.extra_style(),
            extra_script=plugins.registry.extra_script())


def create_page_renderer(template_dir=None, template_name='page.html', env=None):
    if env is None:
        env = jinja2.Environment(loader=jinja2.FileSystemLoader(template_dir))
    renderer = WikiPageRenderer(env.get_template(template_name))
    return renderer


def create_space_renderer(template_dir=None, template_name='index.html', env=None):
    if env is None:
        env = jinja2.Environment(loader=jinja2.FileSystemLoader(template_dir))
    renderer = SpaceRenderer(env.get_template(template_name))
    return renderer


def create_env(template_dir):
    return jinja2.Environment(loader=jinja2.FileSystemLoader(template_dir))

