from .. import minimal, utils

# content_files = [os.path.join('..', 'minimal', cf) for cf in minimal.content_files]
content_files = utils.derive_content_files_from_other_theme(minimal)
build_files = minimal.build_files
