import os, shutil, importlib
path = os.path.dirname(__file__)
templates = [e for e in os.listdir(path)
             if e[0] != '.' and os.path.isdir(os.path.join(path, e))]


def get_theme_base_name(theme):
    try:
        timp = importlib.import_module(f'.{theme}', 'wiking.themes')
        if hasattr(timp, 'base_theme') and timp.base_theme is not None:
            theme = timp.base_theme
    except ImportError:
        pass
    return theme


def copy_extras(template, out_dir, theme_args):
    template_dir = os.path.join(path, template)
    files = os.listdir(template_dir)
    build = None
    try:
        timp = importlib.import_module('.' + template, 'wiking.themes')
        files.extend(timp.content_files)
        files = list(set(files))
        if hasattr(timp, 'build_files'):
            build = timp.build_files
    except ImportError:
        pass
    for f in files:
        do_copy = f.endswith('.css') or f.endswith('.js')
        if do_copy:
            shutil.copy(os.path.join(template_dir, f), out_dir)
    if build is not None:
        build(out_dir, theme_args)
