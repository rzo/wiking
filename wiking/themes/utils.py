import os.path, sass


def derive_content_files_from_other_theme(mod):
    return [os.path.join('..', mod.__name__.split('.')[-1], cf) for cf in mod.content_files]


def compile_sass(out_dir, mod_file, sass_file):
    mod_dir_path = os.path.dirname(mod_file)
    base_name, _ = os.path.splitext(sass_file)
    css = sass.compile(filename=os.path.join(mod_dir_path, sass_file), output_style='compact')
    css_file = os.path.join(out_dir, f'{base_name}.css')
    with open(css_file, 'wt') as fp:
        fp.write(css)


def compile_sass_string(out_dir, mod_file, sass_source, css_file):
    mod_dir_path = os.path.dirname(mod_file)
    css = sass.compile(string=sass_source, include_paths=[mod_dir_path], output_style='compact')
    css_file = os.path.join(out_dir, css_file)
    with open(css_file, 'wt') as fp:
        fp.write(css)
