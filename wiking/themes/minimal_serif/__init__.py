from .. import minimal, utils

# content_files = [os.path.join('..', 'minimal', cf) for cf in minimal.content_files if cf != 'main.css'] + ['main.css']
content_files = utils.derive_content_files_from_other_theme(minimal)
base_theme = 'minimal'


def build_files(dest_dir, theme_args):
    args = {
        'main-font-url': "'https://fonts.googleapis.com/css2?family=Playfair+Display:ital,wght@0,400;0,600;0,700;1,400;1,600;1,700&display=swap'",
        'main-font': "'Playfair Display', Georgia, serif"
    }
    args.update(theme_args)
    minimal.build_files(dest_dir, args)
