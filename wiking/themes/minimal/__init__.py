from .. import utils


content_files = ['prism.js', 'prism.css']


def build_files(dest_dir, theme_args):
    sass_source_lines = []
    for k, v in theme_args.items():
        sass_source_lines.append(f'${k}: {v};')
    sass_source_lines.append('@import "main"\n\n')
    utils.compile_sass_string(dest_dir, __file__,  '\n'.join(sass_source_lines), 'main.css')
