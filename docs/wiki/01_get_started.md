---
title: Get started
---

# Get started

This section will give a quick overview of how to start with new wiking project. It assumes
that following prerequisites are in place:

- installed wiking with `wkng` command line tool in PATH
- text-editor
- browser 

Agenda:

1. [Create new project](create_new_project)
2. [Change project configuration](change_project_configuration)
3. [Build static HTML site](build_static_site)
4. Add new wiki page (page naming and ordering)
5. Add an attachment to page
6. Add and link an image to page
7. Link to anther page
8. Add a sub-page
9. Changing page order


