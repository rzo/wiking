---
title: Wiking
---

# Wiking

_The Wiki Next Gen_

_Wiking_---is a Wiki-based static site generator useful for notes organization, blog articles 
or product documentation.

Wiking uses standard _Markdown_ (via mistune Python library) and provides following features on top of this:
- hierarchical organization into spaces,
- support for attachments to pages (e.g. images linked within wiki page),
- simple file-name based ordering,
- support for wiki file embedded meta-data,
- customizable theming via Jinja2 with SASS support,
- support for a custom block and code-block elements:
    - collapsible block
    - plantuml code-block

