---
title: Build static HTML site
---
# Build static HTML site

To build static HTML site, open your terminal and navigate to `wiki` directory 
(or any of its nested sub-directory) and run following command:

```
wkng build
```

The output as static HTML site will be generated into `built` directory. To view it open
`built/index.html` file in your browser.
