---
title: Create new project
---

# Create new wiki project

First create some empty directory where our project will exist. While being in this directory,
run following command in terminal:

```
wkng init
```

This will create following directory structure:

```
 +-- built          [ here generated static HTML files are stored]
 +-- wiki           [ root for wiki pages ] 
   +-- index.md     [ Root page ]
   +-- info.yml     [ wiki configuration ]
```

## Wiki page and its embedded meta-data

Now you can edit your first page, the `index.md` file. This is the root of you wiki.

If you open it with your text editor, you may see something like this:

```
---
title: wiki
---

# wiki
```

Note the section at the beginning of the file surrounded with `---` lines. 
This section is called _front matter_ and it's the place where meta-data 
associated with specific markdown file are stored. These data will be not rendered,
but are used for other rendering purpose. 

The format of meta-data in front-matter is just `YAML`. Following wiki-page specific 
meta-data are recognized:

- `title`---Title of the page to be used in Wiki project table of content 
  (left side-bar in generated static HTML site)
- `author`---Name of page author (not shown if not filled).

After the front matter, common markdown follows. Note that the page title `wiki` is 
repeated here again. That's because the title meta-data is used only in wiki project 
table of contents, and we want to display the page title also on the page itself.



