---
title: Change project configuration
---
# Change wiki project configuration

In order to change wiki project configuration, you need to edit `info.yml` file in `wiki` directory.

The default project `info.yml` file as created with `wkng init` command looks like this:

```
theme: minimal
title: wiki
```

- `theme`---defines name of theme to be used for generating static HTML site, 
  following themes are included:
    - `minimal`---Sans-serif minimal theme
    - `minimal_serif`---Serif minimal theme
- `title`---project title to be used in navigation elements for the root wiki page.
