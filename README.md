# Wiking

> Wiking: The wiki next-gen

Wiking is simple yet effective wiki-like static-site generator with following goals:
- **organized**
    - each page has its rank prefix making it easy to define order in ToC etc. 
    - each wiki page has its own space (directory) for storing attachments like images,
      or another markdown files as sub-pages,
    - linking images or wiki pages is clever, you don't need to specify whole path 
      or rank prefix, just base file name is enough,
    - each markdown has _front matter_ a little place where meta-data like title 
      (required) are stored in YAML format
- **powerful and extensible**:
  
  Wiking is meant to be extensible following features are 
  included out-of-box:
    - PlantUML support (as `plantuml` code-block)
    - Collapsible box (using `{{collapse}}{{/collapse}}` tags)

