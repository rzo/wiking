---
title: App Store listening
---

# App title

- Colouro Color Scheme Designer
- Colouro Scheme Designer

# Oneliner

- Color scheme designer with unique multi-device instant preview.
- Color scheme designer with instant multi-device preview.
- Design color scheme and preview it instantly on any connected device.

# Selling points

- Instant preview
  - Preview color scheme in real-time using prepared preview patterns.
  - Plug instant preview into your web-design work flow.
- Color rules
  - Common and less common color rules
- Legibility check
  - Make sure the text using colors from scheme is easy to read.
- Share
