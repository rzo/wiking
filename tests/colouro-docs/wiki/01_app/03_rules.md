---
title: Color rules
---
# Color rules

This page provides information on all color rules. Diagrams bellow shows the constraints 
imposed by specific color rule as appear on color wheel.

## Free

This rule (as its name implies) doesn't constraint colors at all. You can choose your colors freely.

## Analogous

![Analogous rule](rule_analogous.svg)

This rule constraints all colors to have same angular distance to each other on color wheel.

## Shades

![Shades rule](rule_shades.svg)

This rule constraints all colors to one color hue and saturation.

## Monochromatic

![Monochromatic rule](rule_mono.svg)

This rule constraints all colors to one color hue.

## Complementary

![Monochromatic rule](rule_compl.svg)

This rule splits color schemes into two groups primary and its complement. In most cases 
primary color is warm and its complement is cold or vice versa.

## Triangle

![Triangle rule](rule_tri.svg)

This rule splits colors into three groups evenly distributed in hue.

## Square

![Square rule](rule_sqr.svg)

This rule splits colors into four groups evenly distributed in hue.

## Rectangle

![Rectangle rule](rule_rect.svg)

This rule splits colors into four groups forming rectangle in color wheel.

## Analogous complement

![Analogous rule](rule_analog_compl.svg)


This rule splits colors into two group. 

- first group with primary color and two its analogous colors (with same angular distance),
- second group with analogous to primary color complement.

