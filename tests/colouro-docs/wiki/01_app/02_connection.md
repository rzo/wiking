---
title: Instant preview troubleshooting
---
# Instant preview troubleshooting

This page helps you troubleshoot problems with connecting to Colouro instant preview.

## Networking overview 

Let's start with quick overview of how the Colouro instant preview works.

<div id="anim" style="width: 65ex; height: 42ex;"></div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bodymovin/5.4.1/lottie.min.js">
</script>
<script>
var animation = bodymovin.loadAnimation({
  container: document.getElementById('anim'),
  renderer: 'svg',
  loop: true,
  autoplay: true,
  path: 'connection/preview_anim.json'
});
</script>

When you start the Colouro app, it runs minimalistic web server on your handset. 
You can then connect to this webserver with any device that can communicate
_directly_ with your handset. 

This autonomous architecture (without centralized backend) 


## Troubleshooting

_TL;DR: Make sure that handset and the preview device are connected to same wireless network._ 

Need for _direct communication_ is the key for successful connection to instant
preview webserver running on handset with Colouro app. If you have troubles connecting
your preview device to Colouro preview webserver, it's likely these devices cannot communicate
directly. Following steps should help you resolve this issue.

**On handset with Colouro app:**

1. In Colouro app: Open preview dialog by tapping on preview icon (toolbar button with wifi symbol).
2. In Preview dialog: Tap on _Troubleshooting_ button.
3. Follow instructions in Troubleshooting dialog, namely:
	1. Verify that the handset has enabled wireless networking.
	2. Verify that the handset is connected to well known wifi network and note its name.

**On preview device:**

1. In OS settings make sure that:
	1. The wifi networking is turned on.
	2. The device is connected to same wifi network as the handset with Colouro app.


