---
title: Colouro app user guide
---
# Colouro app user guide

[Colouro app](https://colouroapp.com/) is a [color scheme](https://en.wikipedia.org/wiki/Color_scheme) 
design app for iOS and Android handsets. This section provides user guide for this app.

## Features

- color rules---constraint scheme colors to follow specific rule (for instance shades or complementary colors).
- contrast check---make sure that text can be easy to read.
- instant preview---see instantly how to color scheme looks like at any connected device nearby.
- SDK for instant preview---plug the instant preview into your web-design workflow; preview the color scheme
  instantly used on your web-site.

## More information

- See [Color rules section](rules) for more details on color scheme rules supported by Colouro.
- See [Connection troubleshooting section](connect) with more details on how to connect to Colouro instant preview.