---
title: Colouro Docs
---

# Welcome to Colouro Docs

Welcome to Colouro Docs site. This is the place where you find all relevant information 
on [Colouro app](https://colouroapp.com/) and how to plug it into your workflow.

## Sections

- [Colouro app user guide](app) gives you complete overview of Colouro app.
- [SDK for JavaScript user guide](sdk-js) provides info on how to plug the Colouro instant preview 
  into your pipeline.

