---
title: Colouro SDK for JavaScript
---
# Plug Colouro into your workflow!

This section provides information on how to plug the Colouro instant preview
into your (not just) web-design workflow.


## Possible approaches

At the moment there are basically three ways you can use instant preview in 
JavaScript based environments:

1. `colouro-reskin.js`: Client-side JavaScript library for quick CSS experiments.

  Pros:
  - Quick and easy way,
  - No dependencies, nothing to download, minimal configuration required.

  Cons:
  - The color scheme is not stored.
  - Does not support CSS processors (SASS, LESS) and their color functions.

2. `colouro-sync` Developer server-side tool.

  Pros:
  - Persistent: the last version of color scheme is saved on developer computer.
  - Integrates well with CSS processors.

  Cons:
  - Requires a bit of configuration and SASS/LESS template to be created.
  - Solution is Node.js-based, which may not be your cup of tea.

3. Low-level access via `Client` classs.

  Pros:
  - Works at any JavaScript platform that supports WebSocket.
  - It is not limitted to web-design! You may use Colouro for anything where
    having color scheme make sense to you. Be it smart RGB light bulb, graphics
    editor plug-in, or even game assets design.

  Cons:
  - You have to write a bit of code that will use `Client` class instance to connect 
    to Colouro preview webserver and handle the `colors` event with your callback.


See [installation section](installation) for more details on getting 
Colouro SDK for JavaScript for your platform.
