---
title: Colouro Reskin
---
# Colouro Reskin

`colouro-reskin.js` is browser-side library providing simple CSS re-skin
feature. After installing `colouro-sdk` package the library is located 
in `<project>/node_modules/colouro-sdk/dist/colouro-reskin.js`

## Getting started

1. Copy `dist/colouro-reskin.js` file to your script location, see 
   [Installation section](installation) for more details.

2. Include the `colouro-reskin.js` script in your HTML file like this:
        
    ```html
    ...
    <head>
       ...
        <script src="colouro-resking.js"></script>
    </head>
    ...
    ```

3.  Next define on-load function to be called when loading is finished.

    ```html
    ...
    <head>
        ...
        <script>
        function onLoad() {
            
            colouro.reskin({
                '#000000':0,  // original primary foreground color
                '#ffffff':1,  // original primary background color
                '#8bfe6f':2,  // original secondary foreground color
                '#141f4e':3,  // original secondary background color
                '#6b0e65':4   // original mid-ground color
            });
        }
        </script>
    </head>
    <body onload="onLoad();">
    ...
    ```

    Replace the color values in defined mappings with your colors as defined
    in stylesheet.

4. Start Colouro app and open the connection dialog.
5. Load the HTML file in browser. In top-right corner there should be small
   frame with _Colouro Host_ URL field. Enter the IP address as shown in connection 
   dialog (without the `http://` prefix) and click the _Connect_ button.
6. Once the Colouro client gets connected to the app, the frame will disappear, 
   and appears again if connection is lost.
   
## Complete example

Following HTML snippet is minimal page example:

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Reskin Test</title>
    <style>
        html, body { margin: 0; }
        .major {
            font-size: 48pt;
            text-align: center;
            padding-top: 20vh;
            color: #000000;
            background-color: #ffffff;
            height: 30vh;
        }
        .minor {
            font-size: 48pt;
            text-align: center;
            color: #8bfe6f;
            background-color: #141f4e;
            height: 30vh;
            padding-top: 10vh;
        }
        .midground {
            height: 10vh;
            background-color: #6b0e65;
        }
    </style>
    <script type="text/javascript" src="js/colouro-reskin.js"></script>
    <script>
        function onLoad() {
            colouro.reskin({
                '#000000':0,
                '#ffffff':1, 
                '#8bfe6f':2, 
                '#141f4e':3, 
                '#6b0e65':4
            });
        }
    </script>
</head>
<body onload="onLoad()">
    <div class="major">Lorem ipsum</div>
    <div class="midground">&nbsp;</div>
    <div class="minor">Lorem ipsum</div>
</body>
</html>

```

