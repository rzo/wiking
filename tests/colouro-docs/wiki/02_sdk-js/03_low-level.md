---
title: Low-level Client API
---
# Low-level Client API

The central component of Colouro SDK is a Colouro client implementation that you
can utilize to plug the Colouro instant preview into you work-flow.

## Getting started

Following snippet shows minimal example of Client class usage, that will print
status and received colors into console.

```js
// Import Colouro SDK (only for node.js based environment, omit this line in browser)
var colouro = require('colouro-sdk'),

    // Semantic name constants 
    semanticNames = ['primary fg', 'primary bg', 'secondary fg', 'secondary bg', 'mg'],
    
    // Colouro app host address
    colouroHost = '192.168.0.42',
    
    // Create client instance with given host and default port
    client = new colouro.Client({host: colouroHost + ':8080'})
        
        // Setup status changed callback
        .on('status', function(connected) { 
            console.log(connected ? 'Connected' : 'Disconnected'); 
        })
        
        // Setup color scheme received callback
        .on('colors', function(colors, semantics) { 
            console.log('Colors received');
            for (var i = 0; i < 5; i++) {
                console.log(
                    i + ': ' + colors[i] + ' (' + semanticNames[semantics[i]] + ')'
                );
            }
        });

```

If you run this script with correct Colouro host, you may see something like this
(hit <kbd>Ctrl</kbd> + <kbd>C</kbd> to quit the script):

```
Connected
Colors received
0: #fcef7a (primary fg)
1: #284e4a (primary bg)
2: #b7fed4 (secondary bg)
3: #2b221e (secondary fg)
4: #ff2a1e (mg)
```

Notes:
1. As you can see the semantic (meaning of color) constants are in range 0..4, 
   see the `semanticNames` in example above.
2. If connecting to colouro host fails, the client will try to reconnect forever. 
   Each time the connection fails or succeeds the `status` callback gets called.
3. If you want to stop the client, call `client.close()`, to start client again
   call `client.init()`.
