---
title: Colouro Sync
---
# Colouro Sync

_Colouro Sync_ is a tool for real-time synchronization of the color scheme 
being edited in Colouro app. Each time color scheme is updated, it gets stored
into defined file.

## Getting started

1. Install `colouro-sdk` package into your node project:
    
    ```
    npm install --save-dev colouro-sdk
    ```
    
2. Create template CSS/SCSS/LESS file like this (Sass example named `scss/colors.scss.in`):
    
        $primary-foreground:     {{0}};
        $primary-background:     {{1}};
        $secondary-foreground:   {{2}};
        $secondary-foreground:   {{3}};
        $middleground:           {{4}};
    
3. In root of your project create `colouro.json` file with this content:

        {
            "host": "192.168.0.42:8080",
            "template-file": "scss/colors.scss.in",
            "dest": "scss/colors.scss"
        }
    
4. In your SASS/LESS stylesheets:
    
    1. Include the file stated in `dest` field of `colouro.json` config,
    2. Use color variables.
    
5. Run Colouro app on your handset, connected to same wireless as the computer with node project.
    
    1. In the application open connect dialog and copy the address of your handset to 
    value of `host` field of `colouro.json` file.
    
6. Open terminal and run following npm command in root of your node project:

    ```
    npx colouro-sync
    ```

7. Run browser-sync task like `gulp dev`. Make sure that the generated file is watched for
changes.


## Template file syntax

In getting started section we created simple Sass template without going into details. 
It may be easy to see that the template uses handlebar syntax with five variables named 
`0` ... `4` being expanded. These variables denote individual color purpose (semantic)
as set in color scheme being edited in Colouro app.

- `{{0}}`---major foreground,
- `{{1}}`---major background,
- `{{2}}`---minor foreground,
- `{{3}}`---minor background,
- `{{4}}`---mid-ground.


`colouro.json` config file
---
Colouro Sync uses simple json config file being located in project root under 
`colouro.json` file with following fields:

- `host` (string): IP address and port (always 8080) with Colouro app server running at handset, 
    e.g. `192.168.0.1:8080`
- `template-file` (string): relative file path of template file to be expanded with current scheme
    colors, e.g. `scss/colors.scss.in`,
- `dest` (string): relative file path of to be generated from template, e.g. `scss/colors.scss`,
- `verbose` (boolean): turn the verbose mode on/off.

## Version control

If your project uses some version control system like `git`, you may wonder what files
you should store in your repository so other team members can use Colouro real-time 
preview (e.g. during brainstorming session).

To share the Colouro workflow, you should store following files in repository:
- template file as stated in `template-file` field of `colouro.json` file,
- generated file as stated in `dest` field of `colouro.json` file,
- `colouro.json` file with configuration.

However storing `colouro.json` is recommend, there's stated a `host` with IP that may
change frequently. To simplify version control, you can create `colouro.local.json` file
in your project root with following content (replace the IP address with your handset 
address as displayed in Colouro app connect dialog):

```json
{
  "host":"10.20.30.40:8080"
}
``` 

The `colouro.local.json` file should marked as ignored for your version system. 
In case of `git` you may add the file into your `.gitignore` like this:

```
echo colouro.local.json >> .gitignore
```

