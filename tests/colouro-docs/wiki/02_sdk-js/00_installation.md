---
title: Installation
---

# Installation

This section provides information on how to get Colouro SDK for JavaScript installed
into your project. Depending on your environment and set of features you'd like to use.

## Web Browser environment

To get [colouro-reskin](colouro-reskin) working in browser do following:

1. (Optional step) Get `dist/colouro-reskin.js` file either:

	- From [public Git repository](https://bitbucket.org/pilsfulab/colouro-sdk-js),
	- From npm package installed to your project, the file is located in 
	  `node_modules/colouro-sdk/dist/colouro-reskin.js`

2. Include the file in your HTML page like this:

	```html
	...
	<head>
		...
		<script src="js/colouro-reskin.js"></script>
	</head>
	...
	```

	Note that the file `js/colouro-reskin.js` referenced in the snippet above is
	the file obtained in step 1. Alternatively you can use on-line hosted package
	using this URL: 

	```
	https://cdn.jsdelivr.net/npm/colouro-sdk@latest/dist/colouro-reskin.js
	```

3. See [colouro-reskin section](colouro-reskin) for usage details.


## Node.js environment

Just install `colouro-sdk` package as development dependency to your node.js project 
like this:

```
npm i -D colouro-sdk
```

Once the package is installed you can:

- Use the `colouro-sync` command via `npx` like this: 

	```
	npx colouro-sync
	```
	
	See [colouro-sync] for more details.

- Get access to Colouro SDK package like this:

	```js
	var colouro = require('colouro-sdk');
	```

	See [Client API section](low-level) for more details.
